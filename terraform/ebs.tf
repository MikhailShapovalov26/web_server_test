resource "aws_ebs_volume" "testebs" {
  availability_zone = aws_instance.vm-shapovalov.availability_zone
  size              = 1
  tags = {
    name = "testebs"
  }
}
resource "aws_volume_attachment" "test_ebs_att" {
  device_name  = "/dev/xvdh"
  volume_id    = aws_ebs_volume.testebs.id
  instance_id  = aws_instance.vm-shapovalov.id
  force_detach = false
  # connection {
  #   type = "ssh"
  #   user = "test"
  #   password = "test"
  #   host = aws_instance.vm-shapovalov.public_ip
  #   port = 22
  # } 
}