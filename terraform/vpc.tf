resource "aws_vpc" "testvpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"
  tags = {
    Name = "testvpc"
  }
}
resource "aws_subnet" "testsubnet" {
  vpc_id                  = aws_vpc.testvpc.id
  cidr_block              = var.subnet_cidr
  availability_zone       = "eu-west-3a"
  map_public_ip_on_launch = true
  tags = {
    name = "testsubnet"
  }
}
resource "aws_subnet" "testsubnet1" {
  vpc_id            = aws_vpc.testvpc.id
  cidr_block        = var.subnet1_cidr
  availability_zone = "eu-west-3a"
  tags = {
    name = "public subnet"
  }
}
resource "aws_subnet" "testsubnet2" {
  vpc_id            = aws_vpc.testvpc.id
  cidr_block        = var.subnet2_cidr
  availability_zone = "eu-west-3b"
  tags = {
    name = "private subnet"
  }
}

resource "aws_internet_gateway" "testgateway" {
  vpc_id = aws_vpc.testvpc.id
}
