terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 4.5.0"
    }
  }
  backend "s3" {
    bucket = "testbucket762"
    encrypt = true
    key = "terraform.tfstate"
    region = "eu-west-3"
  }
}
provider "aws" {
  region = "eu-west-3"
}
resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "aws_key_pair" "ec2_privatekey" {
  key_name   = "testkey"
  public_key = tls_private_key.example.public_key_openssh
  provisioner "local-exec" {
    command = <<-EOT
    echo '${tls_private_key.example.private_key_pem}' > ./testkey.pem
    echo '${tls_private_key.example.public_key_openssh}' > ./testpub.pub
    chmod 600 ./testkey.pem
    chmod 600 ./testpub.pub
    EOT    
  }
}
resource "aws_security_group" "test_ssh" {
  name = "test_ssh"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "test_http" {
  name = "test_http"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}





