resource "aws_elb" "test" {
  name               = "test-elb"
  availability_zones = ["eu-west-3a", "eu-west-3b"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  # listener {
  #   instance_port = 80
  #   instance_protocol = "http"
  #   lb_port = 443
  #   lb_protocol = "https"
  # }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }
  instances                   = [aws_instance.vm-shapovalov.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "test-elb"
  }
}