resource "aws_ecr_repository" "dockerweb2" {
  name                 = "dockerweb2"
  image_tag_mutability = "MUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
}
# resource "aws_ecr_repository" "test_ecr" {
#   name = "test-ecr"
# }

# resource "aws_ecr_repository_policy" "test_ecr" {
#   repository = aws_ecr_repository.dockerweb.name
#   policy     = data.aws_iam_policy_document.describe_ecr_images.json
# }

# data "aws_iam_policy_document" "describe_ecr_images" {
#   statement {
#     sid    = "DescribeImages"
#     effect = "Allow"

#     principals {
#       type        = "AWS"
#       identifiers = [aws_iam_role.test_ecr.arn]
#     }

#     actions = [
#       "ecr:DescribeImages"
#     ]
#   }
# }

# resource "aws_iam_role" "test_ecr" {
#   name = "test_ecr"

#   assume_role_policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "ecr:GetAuthorizationToken",
#                 "ecr:BatchCheckLayerAvailability",
#                 "ecr:GetDownloadUrlForLayer",
#                 "ecr:GetRepositoryPolicy",
#                 "ecr:DescribeRepositories",
#                 "ecr:ListImages",
#                 "ecr:DescribeImages",
#                 "ecr:BatchGetImage",
#                 "ecr:GetLifecyclePolicy",
#                 "ecr:GetLifecyclePolicyPreview",
#                 "ecr:ListTagsForResource",
#                 "ecr:DescribeImageScanFindings",
#                 "ecr:InitiateLayerUpload",
#                 "ecr:UploadLayerPart",
#                 "ecr:CompleteLayerUpload",
#                 "ecr:PutImage"
#             ],
#             "Resource": "*"
#         }
#     ]
# }
# EOF
# }

# resource "aws_iam_role_policy_attachment" "test_ecr_attachment" {
#   role = aws_iam_role.test_ecr.name
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
  
# }