resource "aws_instance" "vm-shapovalov" {
  ami               = "ami-03214270b6681ff5e"
  instance_type     = "t2.micro"
  availability_zone = "eu-west-3a"
  key_name          = aws_key_pair.ec2_privatekey.key_name
  security_groups   = [aws_security_group.test_ssh.name, aws_security_group.test_http.name]

  tags = {
    "name" = "vm-shapovalov"
  }
  connection {
    type        = "ssh"
    user        = "test"
    host        = aws_instance.vm-shapovalov.public_ip
    private_key = file("testkey.pem")
    port        = 22
    timeout     = "3m"
  }
  user_data = "${file("script.sh")}"
}
