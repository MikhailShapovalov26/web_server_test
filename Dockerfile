FROM nginx:1.16.0
USER root
RUN apt-get update -y && apt-get install -y curl awscli
RUN rm /usr/share/nginx/html/*
COPY ./www/ /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]